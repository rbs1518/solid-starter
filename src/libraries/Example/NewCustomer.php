<?php
namespace Example;
use Exception;

class NewCustomer extends Customer
{
    public function Add(Database $db)
    {
        try
        {
            $db->Read();
            $db->Process();
            $db->Add();
        }
        catch (Exception $x)
        {
            $this->Log->Log($x->getMessage());
        }
    }

}
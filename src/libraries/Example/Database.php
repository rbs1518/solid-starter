<?php
namespace Example;

class Database
{
    public function Add()
    {
        echo "DummyDB: Add function called.\n";
    }

    public function Process()
    {
        echo "DummyDB: Process function called.\n";
    }

    public function Read()
    {
        echo "DummyDB: Read function called.\n";
    }
}
?>
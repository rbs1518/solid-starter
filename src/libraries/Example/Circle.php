<?php
namespace Example;

class Circle implements IShape
{
    public $Radius;

    public function __construct($radius)
    {
        $this->Radius = $radius;
    }

    public function GetArea()
    {
        return (3.14 * ($this->Radius * $this->Radius));
    }
}
?>
<?php
namespace Example;
use Exception;

class Customer
{
    public $Type = 0;

    /**
     * @var ILogger|null
     */
    public $Log = null;

    public function __construct(ILogger $log)
    {
        $this->Log = $log;
    }

    public function Add(Database $db)
    {
        try
        {
            $db->Add();
        }
        catch (Exception $x)
        {
            $this->Log->Log($x->getMessage());
        }
    }
}
?>
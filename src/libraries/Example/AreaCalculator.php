<?php
namespace Example;

class AreaCalculator implements IOutputable
{
    /**
     * @var IShape[]
     */
    private $_Shapes;

    public function __construct($shapes)
    {
        $this->_Shapes = $shapes;
    }

    public function Sum()
    {
        $sum = 0;
        foreach ($this->_Shapes as $shape)
        {
            $sum += $shape->GetArea();
        }

        return $sum;
    }

    /*public function Output()
    {
        return "Sum of the areas of provided shapes: ".$this->Sum()."\n";
    }*/

    function GetOutput()
    {
        return $this->Sum();
    }
}
?>
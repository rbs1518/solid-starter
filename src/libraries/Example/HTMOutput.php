<?php
namespace Example;


class HTMOutput implements IOutputProcess
{
    /**
     * @var null|IOutputable
     */
    private $Object = null;

    public function __construct(IOutputable $object)
    {
        $this->Object = $object;
    }

    public function Output()
    {
        return "<p>".$this->Object->GetOutput()."</p>";
    }
}
?>
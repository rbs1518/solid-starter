<?php
namespace Example;

class Square implements IShape
{
    public $Length;

    public function __construct($length)
    {
        $this->Length = $length;
    }

    public function GetArea()
    {
        return ($this->Length * $this->Length);
    }
}
?>
<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see readme.md.
 */

namespace
{
    define('SELIFA_EXCEPTION_SYSTEM_START_CODE',9000);
    define('SELIFA_EXCEPTION_START_CODE', 10000);
    define('SELIFA_EXCEPTION_NULL', SELIFA_EXCEPTION_SYSTEM_START_CODE + 11);
    define('SELIFA_EXCEPTION_SYSTEM', SELIFA_EXCEPTION_SYSTEM_START_CODE + 12);
    define('SELIFA_EXCEPTION_UNDEFINED', SELIFA_EXCEPTION_SYSTEM_START_CODE + 13);
    define('SELIFA_EXCEPTION_CODE_OUT_OF_RANGE', SELIFA_EXCEPTION_SYSTEM_START_CODE + 14);
    define('SELIFA_EXCEPTION_INTERNAL', SELIFA_EXCEPTION_SYSTEM_START_CODE + 15);

    define('SELIFA_EXCEPTION_NON_ZERO_IDENTIFIER', SELIFA_EXCEPTION_START_CODE + 11);
    define('SELIFA_EXCEPTION_PARAMETER_IS_MISSING', SELIFA_EXCEPTION_START_CODE + 12);
    define('SELIFA_EXCEPTION_VALUE_IS_MISSING', SELIFA_EXCEPTION_START_CODE + 13);

    /**
     * Class SelifaInternalException
     * @copyright RBS 2014
     * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
     * @package SelifaLiteCore
     */
    class SelifaInternalException extends Exception
    {
        /**
         * @param string $message
         * @param Exception|null $previousException
         */
        public function __construct($message = '', $previousException = null)
        {
            $eCode = SELIFA_EXCEPTION_INTERNAL;
            parent::__construct($message, $eCode, $previousException);
        }
    }

    /**
     * Class SelifaException
     * @copyright RBS 2014
     * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
     * @package SelifaLiteCore
     */
    class SelifaException extends Exception
    {
        /**
         * @var array
         */
        public $ArrayData;

        /**
         * @param int $eCode
         * @param string $message
         * @param array $arrayData
         * @param Exception|null $previousException
         */
        public function __construct($eCode, $message = '', $arrayData = array(), $previousException = null)
        {
            parent::__construct($message, $eCode, $previousException);
            $this->ArrayData = $arrayData;
        }

        /**
         * @param array $aData
         */
        public function AppendData($aData)
        {
            $this->ArrayData = array_merge_recursive($this->ArrayData,$aData);
        }
    }
}

namespace RBS\Selifa
{
    use \Exception;
    use \SelifaException;
    use \SelifaInternalException;

    /**
     * Class XM
     * @package RBS\Selifa
     */
    class XM implements IComponent
    {
        /**
         * @var XM
         */
        private static $_Instance = null;

        /**
         * @var bool
         */
        private $_ShowExceptionTrace = false;

        /**
         * @var bool
         */
        private $_VerboseInternalException = false;

        /**
         * @var bool
         */
        private $_VerboseSystemException = false;

        /**
         * @var null|callable
         */
        private $_TranslateFunction = null;

        /**
         * @var null|callable
         */
        private $_LogFunction = null;

        /**
         * @param array $options
         * @return XM
         */
        public static function Initialize($options)
        {
            if (self::$_Instance == null)
                self::$_Instance = new XM($options);
            return self::$_Instance;
        }

        /**
         * @param array $options
         */
        private function __construct($options)
        {
            if (isset($options['EnableTrace']))
                $this->_ShowExceptionTrace = (bool)$options['EnableTrace'];
            if (isset($options['VerboseInternalException']))
                $this->_VerboseInternalException = (bool)$options['VerboseInternalException'];
            if (isset($options['VerboseSystemException']))
                $this->_VerboseSystemException = (bool)$options['VerboseSystemException'];
            if (isset($options['TranslateFunction']))
            {
                if (is_callable($options['TranslateFunction']))
                    $this->_TranslateFunction = $options['TranslateFunction'];
            }
            if (isset($options['LogFunction']))
            {
                if (is_callable($options['LogFunction']))
                    $this->_LogFunction = $options['LogFunction'];
            }
        }

        /**
         * @param Exception $x
         * @return array|null
         */
        public function ExceptionToArray(Exception $x)
        {
            if ($x != null)
            {
                return array(
                    'Code' => $x->getCode(),
                    'Message' => $x->getMessage(),
                    'Line' => $x->getLine(),
                    'File' => $x->getFile(),
                    'Trace' => $x->getTrace(),
                    'PreviousException' => $this->ExceptionToArray($x->getPrevious())
                );
            }
            return null;
        }

        /**
         * Set the function that will be used for translating error code into human-readable message.
         * Function signature:
         *      string function($code,[$data],[$exception])
         *      - $code integer, an error code from exception object.
         *      - $data array, optional, an array containing data passed when throwing an SelifaException object.
         *      - $exception Exception, the exception object itself.
         *      - returns string, the translate function must return a string.
         *
         * @param callable|null $translateFunction
         */
        public static function SetTranslateFunction($translateFunction)
        {
            if ((self::$_Instance != null) && (is_callable($translateFunction)))
                self::$_Instance->_TranslateFunction = $translateFunction;
        }

        /**
         * Set the function that will be used for logging exception.
         * Function signature:
         *      void function($exception,[$data])
         *      - $exception Exception, the exception object itself.
         *      - $data array, an array containing translated exception data.
         *
         * @param callable|null $logFunction
         */
        public static function SetLogFunction($logFunction)
        {
            if ((self::$_Instance != null) && is_callable($logFunction))
                self::$_Instance->_LogFunction = $logFunction;
        }

        /**
         * @param Exception|SelifaException|SelifaInternalException $x
         * @return array
         */
        public static function HandleException($x)
        {
            $instance = self::$_Instance;
            $result = array('X' => null, 'M' => '', 'C' => SELIFA_EXCEPTION_NULL);

            if ($instance != null)
            {
                if ($x instanceof SelifaException)
                {
                    $eCode = (int)$x->getCode();
                    $result['C'] = $eCode;

                    if (($eCode > SELIFA_EXCEPTION_START_CODE) || ($instance->_TranslateFunction != null))
                    {
                        $tFunction = $instance->_TranslateFunction;
                        $s = $tFunction($eCode,$x->ArrayData,$x);
                        $result['M'] = $s;
                    }
                    else
                    {
                        $result['M'] = $x->getMessage();
                    }
                }
                else
                {
                    if ($x instanceof SelifaInternalException)
                    {
                        $result['C'] = (int)$x->getCode();
                        if ($instance->VerboseInternalException)
                            $result['M'] = $x->getMessage();
                        else
                            $result['M'] = 'Internal exception occurs.';
                    }
                    else
                    {
                        $result['C'] = SELIFA_EXCEPTION_SYSTEM;
                        if ($instance->VerboseSystemException)
                        {
                            $s = 'Exception at line ' . $x->getLine() . ' at file ' . $x->getFile() . ', ' . $x->getMessage();
                            if ($instance->ShowExceptionTrace)
                                $result['X'] = $instance->ExceptionToArray($x);
                            $result['M'] = $s;
                        }
                        else
                        {
                            $result['M'] = 'System exception occurs.';
                        }
                    }
                }
            }

            if ($instance->_LogFunction != null)
                $instance->_LogFunction($x,$result);
            return $result;
        }

        /**
         * @throws SelifaException
         */
        public static function ThrowError()
        {
            $args = func_get_args();
            if (count($args) > 1)
            {
                $eMessage = '';
                $eCode = SELIFA_EXCEPTION_UNDEFINED;
                if (is_string($args[0]))
                    $eMessage = (string)$args[0];
                else
                {
                    if (is_integer($args[0]))
                        $eCode = (int)$args[0];
                }

                unset($args[0]);
                throw new SelifaException($eCode, $eMessage, $args);
            }
            else
            {
                if (count($args) > 0)
                {
                    $eMessage = '';
                    $eCode = SELIFA_EXCEPTION_UNDEFINED;
                    if (is_string($args[0]))
                        $eMessage = (string)$args[0];
                    else
                    {
                        if (is_integer($args[0]))
                            $eCode = (int)$args[0];
                    }
                    throw new SelifaException($eCode, $eMessage);
                }
            }
        }

        /**
         * @param string $message
         * @throws SelifaInternalException
         */
        public static function ThrowInternalError($message = '')
        {
            throw new SelifaInternalException($message, null);
        }
    }
}
?>
<?php
define('SELIFA_ROOT_PATH',dirname(__FILE__));
define('SELIFA_TIME_STARTED',microtime(true));
define('SELIFA','v1.0');
define('SELIFA_NAME','Selifa');
include(SELIFA_ROOT_PATH.'/selifa/core/Core.php');

/**
 * Dump $var to error log.
 *
 * @param $var
 */
function dumpToErrorLog($var)
{
    ob_start();
    var_dump($var);
    $s = ob_get_contents();
    ob_end_clean();
    error_log('DUMP: '.$s,0);
}
?>
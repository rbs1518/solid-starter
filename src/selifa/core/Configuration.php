<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see readme.md.
 */

namespace RBS\Selifa;

/**
 * Interface IConfigurationExtension
 * @package RBS\Selifa
 */
interface IConfigurationExtension
{
    /**
     * @return string
     */
    public function GetConfigurationID();

    /**
     * @param string $configFileName
     * @param string $className
     * @return array
     */
    public function GetConfigurationData($configFileName,$className);
}

/**
 * Configuration manager used by selifa core to handle component's configuration.
 *
 * @package RBS\Selifa
 */
class Configuration
{
    /**
     * @var null|Configuration
     */
    private static $_Instance = null;

    /**
     * @var string
     */
    private $_DefaultConfigDir = '';

    /**
     * @var string
     */
    private $_AppConfigDir = '';

    /**
     * @var null|IConfigurationExtension
     */
    private $_Extension = null;

    /**
     * Register a configuration extension.
     * Configuration extension can be used to extend functionality of configuration manager or enables
     * custom configuration routine.
     *
     * This one is a static function.
     *
     * @param IConfigurationExtension $extension
     */
    public static function RegisterExtension(IConfigurationExtension $extension)
    {
        if (self::$_Instance != null)
            self::$_Instance->_Extension = $extension;
    }

    /**
     * @param string $rootDir
     * @param string $appConfigDir
     */
    private function __construct($rootDir,$appConfigDir)
    {
        $this->_DefaultConfigDir = ($rootDir.'defaults/');
        $this->_AppConfigDir = $appConfigDir;
    }

    /**
     * @param string $id
     * @param array $data
     */
    private function _CacheData($id,$data)
    {

    }

    /**
     * Register a configuration extension.
     * Configuration extension can be used to extend functionality of configuration manager or enables
     * custom configuration routine.
     *
     * @param IConfigurationExtension $extension
     */
    public function SetExtension(IConfigurationExtension $extension)
    {
        $this->_Extension = $extension;
    }

    /**
     * Load configuration for a component based on it's classname.
     * First, it will try to load a default configuration, then load from application's configuration path.
     * If an extension is registered, then it will try to call GetConfigurationData from extension object.
     *
     * @param string $className The component class name.
     * @return array|mixed
     */
    public function LoadForComponent($className)
    {
        if ($this->_Extension != null)
        {
            $temp = array();
            $configFileName = strtolower(str_replace("\\",'_',$className));
            $configId = ($configFileName.'__'.$this->_Extension->GetConfigurationID());

            $filePath = ($this->_DefaultConfigDir.$configFileName.'.php');
            if (file_exists($filePath))
                $temp = include($filePath);
            $aFilePath = ($this->_AppConfigDir.$configFileName.'.php');
            if (file_exists($aFilePath))
                $temp = array_replace_recursive($temp,include($aFilePath));

            $xOpts = $this->_Extension->GetConfigurationData($configFileName,$className);
            if ($xOpts != null)
                $temp = array_replace_recursive($temp,$xOpts);

            $this->_CacheData($configId,$temp);
            return $temp;
        }
        else
        {
            $temp = array();
            $configFileName = strtolower(str_replace("\\",'_',$className));
            $filePath = ($this->_DefaultConfigDir.$configFileName.'.php');
            if (file_exists($filePath))
                $temp = include($filePath);
            $aFilePath = ($this->_AppConfigDir.$configFileName.'.php');
            if (file_exists($aFilePath))
                $temp = array_replace_recursive($temp,include($aFilePath));

            $this->_CacheData($configFileName,$temp);
            return $temp;

        }
    }

    /**
     * Initialize configuration manager object.
     *
     * @param string $rootDir
     * @param string $appConfigDir
     * @return null|Configuration
     */
    public static function Initialize($rootDir='',$appConfigDir='')
    {
        if (self::$_Instance == null)
            self::$_Instance = new Configuration($rootDir,$appConfigDir);
        return self::$_Instance;
    }

    /**
     * @param string $keyName
     * @param mixed|null $defaultValue
     * @param bool $asBoolean
     * @return mixed|null
     */
    public static function Env($keyName,$defaultValue=null,$asBoolean=false)
    {
        $value = getenv($keyName,true)?:(getenv($keyName)?:$defaultValue);
        if ($asBoolean)
            return (strtolower(trim($value)) == 'true');
        else
        {
            if (($value === "''") || ($value === '""'))
                return '';
            else
                return $value;
        }
    }
}
?>
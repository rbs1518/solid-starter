<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see readme.md.
 */

namespace RBS\Selifa;

/**
 * Class Core
 *
 * Selifa Core process. This class replace old selifalite's GCPM clase.
 * Different from GCPM which keep a list of predefined class paths,
 * this class does not need a list of class paths, and it will load any components or classes
 * directly from their respective folder.
 *
 * There are two kind of classes supported. They are Library Classes, and Component Classes.
 * Library clases are typical classes, while Component classes granted the ability to load a configuration.
 * They must implement IComponent interface.
 *
 * @package RBS\Selifa
 */
class Core
{
    /**
     * @var null|Core
     */
    private static $_Instance = null;

    /**
     * @var string
     */
    private $_RootDir = '';

    /**
     * @var string
     */
    private $_CoreDir = '';

    /**
     * @var string
     */
    private $_ComponentDir = '';

    /**
     * @var string
     */
    private $_LibraryDir = '';

    /**
     * @var string
     */
    private $_DriverDir = '';

    /**
     * @var null|Configuration
     */
    private $_ConfigObject = null;

    /**
     * @var array
     */
    private $_ClassMap = array();

    /**
     * @var boolean
     */
    public $_UseComposer = false;

    /**
     * @param array $options
     */
    private function __construct($options)
    {
        if (isset($options['RootPath']))
            $this->_RootDir = ($options['RootPath'].'/');
        $selifaDir = ($this->_RootDir.'selifa');
        if (isset($options['SelifaPath']))
            $selifaDir = ($this->_RootDir.$options['SelifaPath']);
        $this->_CoreDir = (dirname(__FILE__).'/');
        require_once($this->_CoreDir.'Configuration.php');
        require_once($this->_CoreDir.'IComponent.php');

        $configDir = ($this->_RootDir.'secured');
        if (isset($options['ConfigDir']))
            $configDir = ($this->_RootDir.$options['ConfigDir']);
        $this->_ConfigObject = Configuration::Initialize(($selifaDir.'/'),($configDir.'/'));

        $this->_ComponentDir = ($this->_RootDir.'components/');
        if (isset($options['ComponentPath']))
            $this->_ComponentDir = ($this->_RootDir.$options['ComponentPath'].'/');
        define('SELIFA_COMPONENT_PATH',$this->_ComponentDir);

        $this->_LibraryDir = ($this->_RootDir.'libraries/');
        if (isset($options['LibraryPath']))
            $this->_LibraryDir = ($this->_RootDir.$options['LibraryPath'].'/');
        define('SELIFA_LIBRARY_PATH',$this->_LibraryDir);

        $this->_DriverDir = ($this->_RootDir.'drivers/');
        if (isset($options['DriverPath']))
            $this->_DriverDir = ($this->_RootDir.$options['DriverPath'].'/');
        define('SELIFA_DRIVER_PATH',$this->_DriverDir);

        if (isset($options['UseComposer']))
            $this->_UseComposer = (bool)$options['UseComposer'];

        spl_autoload_register(array('\RBS\Selifa\Core','XLibraryClassLoader'));
        if ($this->_UseComposer)
        {
            if (isset($options['ComposerVendorPath']))
                $autoloadFile = ($this->_RootDir.'/'.$options['ComposerVendorPath'].'/autoload.php');
            else
                $autoloadFile = ($this->_RootDir.'/vendor/autoload.php');
            if (file_exists($autoloadFile))
                include($autoloadFile);
        }
    }

    /**
     * Initializes Selifa Core class with the given options.
     *
     * @param null|array $options {
     *      @var string $RootPath Full path of root selifa directory (required).
     *      @var string $SelifaPath Core selifa path relative to root selifa directory (optional, default: 'selifa').
     *      @var string $ConfigDir Configuration directory relative to root selifa directory (optional, default: 'secured').
     *      @var string $ComponentPath Component directory relative to root selifa directory (optional, default: 'components').
     *      @var string $LibraryPath Library directory relative to root selifa directory (optional, default: 'libraries').
     *      @var string $DriverPath Driver directory relative to root selifa directory (optional, default: 'drivers').
     *      @var array LoadComponents An array to full classname that need to be loaded at initialization.
     * }
     * @return null|Core
     * @throws \Exception
     */
    public static function Initialize($options=null)
    {
        if (self::$_Instance == null)
        {
            self::$_Instance = new Core($options);
            if (isset($options['LoadComponents']))
            {
                $loadComponents = $options['LoadComponents'];
                if (is_array($loadComponents))
                {
                    foreach ($loadComponents as $className)
                        if (!class_exists($className,true))
                            throw new \Exception("Class ".$className." is not loaded.");
                }
            }
        }
        return self::$_Instance;
    }

    /**
     * Map a class that resides different or outside libraries/components.
     *
     * @param string $className
     * @param string $path
     * @param string $nameSpace
     */
    public static function MapClass($className,$path,$nameSpace='')
    {
        $ns = '';
        if ($nameSpace != '')
            $ns = ($nameSpace."\\");

        if (is_array($className))
            foreach ($className as $cn)
                self::$_Instance->_ClassMap[$ns.$cn] = ($path.$cn.'.php');
        else if (is_string($className))
            self::$_Instance->_ClassMap[$ns.$className] = ($path.$className.'.php');
    }

    /**
     * Autoloader function for mapped classes.
     *
     * @param string $className
     * @return bool
     */
    public static function ClassMapLoader($className)
    {
        if (isset(self::$_Instance->_ClassMap[$className]))
        {
            $classFile = self::$_Instance->_ClassMap[$className];
            if (!file_exists($classFile))
                return false;
            require_once($classFile);
            return true;
        }
        return false;
    }

    /**
     * Default autoloader for selifa core.
     *
     * @param string $className
     * @return bool
     */
    public static function XLibraryClassLoader($className)
    {
        $fileName = str_replace("\\",'/',$className);
        $fullPathName = (self::$_Instance->_LibraryDir.$fileName.'.php');
        if (!file_exists($fullPathName))
            return false;
        require_once($fullPathName);
        if (is_a($className,'RBS\Selifa\IComponent',true))
        {
            $x = self::$_Instance->_ConfigObject->LoadForComponent($className);
            $isEnabled = true;
            if (isset($x['Enabled']))
                $isEnabled = $x['Enabled'];
            if (!$isEnabled)
                return false;
            $x['DriverPath'] = self::$_Instance->_DriverDir;
            $className::Initialize($x);
        }
        return true;
    }

    /**
     * Returns elapsed processing time in microseconds.
     * Note: you must need to define const SELIFA_TIME_STARTED at the beginning of your script/app.
     * Example: Put this "define('SELIFA_TIME_STARTED',microtime(true));" at very first of your script.
     *
     * @return float
     */
    public static function GetElapsedProcessingTime()
    {
        return ((float)microtime(true) - (float)SELIFA_TIME_STARTED);
    }

    /**
     * Output resource usage (elapsed time ana memory usage).
     */
    public static function WriteResourceUsage()
    {
        $elapsed = self::GetElapsedProcessingTime();
        echo "\n\nTime elapsed: ".$elapsed." second(s).";
        $mUsage = (memory_get_usage(false) / 1024);
        echo "\nMemory: ".$mUsage." KB(s).";
    }
}
?>
<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see readme.md.
 */

/**
 * Loader File for Selifa 1.0
 * Previously known as SelifaLite 2.0, changed due to different concept (Again ???)
 * Instead of differentiate between any core components (database, parsers, providers, etc),
 * Selifa 1.0 will use same component interface.
 *
 * The use of this file is entirely optional. One can create custom loader for selifa.
 * Just load selifa/core/Core.php file and call Core::Initialize.
 *
 * Copyright(c) 2015-2017. Rinardi Budiadi Sarean
 *
 * @copyright RBS 2015-2017
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 * @package Selifa
 */

define('SELIFA_TIME_STARTED',microtime(true));
define('SELIFA','v1.0');
define('SELIFA_NAME','Selifa');

$siteRoot = $_SERVER['DOCUMENT_ROOT'];
if (defined('SELIFA_ROOT_PATH'))
    $siteRoot = SELIFA_ROOT_PATH;

$currentPath = dirname(__FILE__);
include($currentPath.'/core/Core.php');
\RBS\Selifa\Core::Initialize(array(
    'RootPath' => $siteRoot,
    'LoadComponents' => array(
        '\RBS\Selifa\XM'
    )
));

?>
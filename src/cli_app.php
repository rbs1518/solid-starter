<?php
include('init.php');
use RBS\Selifa\Core as SelifaCore;
SelifaCore::Initialize(array(
    'RootPath' => SELIFA_ROOT_PATH,
    'ConfigDir' => 'configs',
    'UseComposer' => true,
    'LoadComponents' => array(
        '\RBS\Selifa\XM'
    )
));

use Example\Circle;
use Example\Square;
use Example\AreaCalculator;
use Example\HTMOutput;


echo "Hello World!\n";


$shapes = array(
    new Circle(20),
    new Square(30),
    new Circle(10)
);

$calc = new AreaCalculator($shapes);

$html = new HTMOutput($calc);

echo $html->Output();


?>